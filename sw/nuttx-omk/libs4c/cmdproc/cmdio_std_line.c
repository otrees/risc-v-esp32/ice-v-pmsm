#include <cmd_proc.h>
#include <string.h>

#define ED_LINE_CHARS 80

char ed_line_in_std[ED_LINE_CHARS+1];
char ed_line_out_std[ED_LINE_CHARS+1];


ed_line_buf_t ed_line_buf_in_std={
//    flg:FL_ELB_ECHO,
    inbuf:0,
    alloc:sizeof(ed_line_in_std),
    maxlen:0,
    lastch:0,
    buf:ed_line_in_std
};

ed_line_buf_t ed_line_buf_out_std={
    flg:FL_ELB_NOCRLF,
    inbuf:0,
    alloc:sizeof(ed_line_out_std),
    maxlen:0,
    lastch:0,
    buf:ed_line_out_std
};

extern cmd_io_t cmd_io_std;
const cmd_io_t cmd_io_std_line={
    putc:cmd_io_line_putc,
    getc:NULL,
    write:cmd_io_write_bychar,
    read:NULL,
    priv:{
        ed_line:{
            in: &ed_line_buf_in_std,
            out:&ed_line_buf_out_std,
            io_stack:&cmd_io_std
        }
    }
};
