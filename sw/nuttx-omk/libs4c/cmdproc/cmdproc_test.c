#include <cmd_proc.h>
#include <stdio.h>

int cmd_do_testopchar(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;

  opchar=cmd_opchar_check(cmd_io,des,param);
  if(opchar<0) return opchar;

  printf(
      "cmd_do_testopchar called\n"
      "param[0]=%s\n"
      "param[1]=%s\n"
      "param[2]=%s\n"
      "param[3]=%s\n"
      "opchar=%c\n", 
      param[0], param[1], param[2], param[3], opchar);

  return 0;
}

int cmd_do_testparam(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  printf(
      "cmd_do_testparam called\n"
      "param[0]=%s\n"
      "param[1]=%s\n"
      "param[2]=%s\n",
      param[0], param[1], param[2]);

  return 0;
}

int cmd_do_testerror(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  return -1234;
}

int cmd_do_test(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
    printf("This is the simplest command\n");
    return 0;
}

int cmd_do_testcmdio(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
    cmd_io_puts(cmd_io, "The first line of text\n");
    cmd_io_puts(cmd_io, "The second line of text\n");
    /* Only ED_LINE_CHARS character can be sent. */
    return 0;
}

cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help={
    0, 0,
    "HELP","prints help for commands",
    cmd_do_help,{(char*)&cmd_list}};

int val;
cmd_des_t const cmd_des_val={
    0, CDESM_OPCHR|CDESM_RW,
    "VAL","use ':' or '?' to store/read value of an integer variable",
    cmd_do_rw_int, {(char*)&val}};

cmd_des_t const cmd_des_valro={
    0, CDESM_OPCHR|CDESM_RD,
    "VALRO","read only access to an integer variable",
    cmd_do_rw_int, {(char*)&val}};

cmd_des_t const cmd_des_valwo={
    0, CDESM_OPCHR|CDESM_WR,
    "VALWO","write only access to an integer variable",
    cmd_do_rw_int, {(char*)&val}};

cmd_des_t const cmd_des_opchar_test={
    0, CDESM_OPCHR|CDESM_RW,
    "OPCHAR","opchar test (use ':' or '?' as suffix)",
    cmd_do_testopchar};

cmd_des_t const cmd_des_opchar_testro={
    0, CDESM_OPCHR|CDESM_RD,
    "OPCHARRO","opchar test (only '?' is allowed)",
    cmd_do_testopchar};

cmd_des_t const cmd_des_test={
    0, 0,
    "TEST","the simplest command",
    cmd_do_test};

cmd_des_t const cmd_des_testio={
    0, 0,
    "TESTIO","test of cmd_io inside functions (universal way to print results)",
    cmd_do_testcmdio};

cmd_des_t const cmd_des_param={
    0, 0,
    "PARAM","test of parameters",
    cmd_do_testparam};

cmd_des_t const cmd_des_prefix={
    0, 0,
    "PREFIX*","suffix of the command is supplied as a parametr",
    cmd_do_testparam};

cmd_des_t const cmd_des_num={
    0, 0,
    "NUM##","suffix of the command (two digits) is supplied as a parametr",
    cmd_do_testparam};

cmd_des_t const cmd_des_char={
    0, 0,
    "CHAR?","suffix of the command (one character) is supplied as a parametr",
    cmd_do_testparam};

cmd_des_t const cmd_des_charmid={
    0, 0,
    "CHAR?MID","middle character of the command is supplied as a parametr",
    cmd_do_testparam};

cmd_des_t const cmd_des_hiddedn={
    0, 0,
    "HIDDEN","should not be available",
    cmd_do_test};

cmd_des_t const cmd_des_error={
    0, 0,
    "TESTERROR","should produce an error",
    cmd_do_testerror};

/* Command lists */

cmd_des_t const *cmd_list_1[]={
    &cmd_des_val,
    &cmd_des_valro,
    &cmd_des_valwo,
    &cmd_des_opchar_test,
    &cmd_des_opchar_testro,
    NULL
    };

cmd_des_t const *cmd_list_2[]={
    &cmd_des_test,
    &cmd_des_testio,
    &cmd_des_param,
    &cmd_des_prefix,
    &cmd_des_num,
    &cmd_des_char,
    &cmd_des_charmid,
    NULL
    };

cmd_des_t const *cmd_list_main[]={
  &cmd_des_help,
  &cmd_des_error,
  CMD_DES_INCLUDE_SUBLIST(cmd_list_1),
  CMD_DES_CONTINUE_AT(cmd_list_2),
  &cmd_des_hiddedn,
  NULL
};

cmd_des_t const **cmd_list = cmd_list_main;

cmd_io_t cmd_io_std_line;

int main()
{
    while (1) {
        cmd_processor_run(&cmd_io_std_line, cmd_list_main);
    }
}
