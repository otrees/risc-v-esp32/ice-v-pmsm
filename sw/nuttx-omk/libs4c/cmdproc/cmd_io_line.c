/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_proc.h - text line command processor
               designed for instruments control and setup
	       over RS-232 line
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com
            (C) 2007 by Michal Sojka <sojkam1@fel.cvut.cz>

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <cmd_proc.h>
#include "cmd_proc_priv.h"
#include <string.h>

/* cmd_io line editor */

/** 
 * Adds new characters to an edit line buffer.
 * 
 * @param elb Edit line buffer.
 * @param ch character to add.
 * 
 * @return 1 in case of end of line, 0 otherwise.
 */
int cmd_ed_line_buf(ed_line_buf_t *elb, int ch)
{
  int lastch=elb->lastch;
  if (elb->flg&FL_ELB_INSEND)
    return -1;
  elb->lastch=ch;
  if(!lastch){
    elb->inbuf=0;               /* Start new line */
  }
  if((!(elb->flg&FL_ELB_NOCRLF))&&((ch=='\n')||(ch=='\r'))){
    if((lastch=='\n')&&(ch=='\r')) /* Empty line, ignore it. */
      return 0;
    elb->lastch=0;               /* End the string */
    elb->buf[elb->inbuf]=0;
    return 1;
  }
  if(elb->inbuf>=elb->alloc-1){ 
    /* try to reallocate buffer len not implemented */
    return 0;
  }
  elb->buf[elb->inbuf++]=ch;  
  return 0;
}

int cmd_io_line_putc(cmd_io_t *cmd_io,int ch)
{
  return cmd_ed_line_buf(cmd_io->priv.ed_line.out,ch);
}

/* Process pending output */
int cmd_io_line_out(cmd_io_t *cmd_io)
{
  cmd_io_t* io_stack=cmd_io->priv.ed_line.io_stack;
  ed_line_buf_t* ed_line_out=cmd_io->priv.ed_line.out;
  
  if(!ed_line_out->inbuf) return 0;
  if(!io_stack)
    return -1;
  
  if(!(ed_line_out->flg&FL_ELB_INSEND)){
    ed_line_out->flg|=FL_ELB_INSEND;
    ed_line_out->lastch=0;
  }
  while(cmd_io_putc(io_stack, ed_line_out->buf[ed_line_out->lastch])>=0){
    if(++ed_line_out->lastch >= ed_line_out->inbuf){
      ed_line_out->lastch=0;
      ed_line_out->inbuf=0;
      ed_line_out->flg&=~FL_ELB_INSEND;
      return 0;
    }
  }
  return 1;
}

/* process input */
int cmd_io_line_in(cmd_io_t *cmd_io)
{
  int ch;
  cmd_io_t* io_stack = cmd_io->priv.ed_line.io_stack;
  ed_line_buf_t *ed_line_in = cmd_io->priv.ed_line.in;

  if(!io_stack)
      return -1;

  while((ch=cmd_io_getc(io_stack))>=0){
//    DPRINT("Added %c (%d)\n", ch, ch);
    int eol = cmd_ed_line_buf(ed_line_in,ch);
    if(eol){
      if(ed_line_in->flg&FL_ELB_ECHO){
        while(cmd_io_putc(io_stack,'\r')<0);
        while(cmd_io_putc(io_stack,'\n')<0);
      }
      return 1;
    }
    else 
      if(ed_line_in->flg&FL_ELB_ECHO) {
        while(cmd_io_putc(io_stack,ch)<0);
      }
  }
  return 0;
}

/* The possibly blocking read of one line, should be used only
   when other options fails */
char *cmd_io_line_rdline(cmd_io_t *cmd_io, int mode)
{
  int ret;
  while((ret=cmd_io_line_in(cmd_io))==0)
    if(!mode) break;
  if(ret<=0) return NULL;
  return cmd_io->priv.ed_line.in->buf;
}

/* Local Variables: */
/* c-basic-offset: 2 */
/* End */
