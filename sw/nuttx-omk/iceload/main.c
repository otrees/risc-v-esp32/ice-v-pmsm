#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>

#include <nuttx/spi/ice40.h>

int main(int argc, char *argv[]) {
    printf("Load demo for the ICE40 FPGA\n");

    const char *bstr_fname = "/etc/fpga/blinky.bin";
    const char *ice40_dev = "/dev/ice40";

    if (argc >= 2) {
        bstr_fname = argv[1];
    }

    int ret;

    bool invalue;

    FILE *file;
    char *buffer;
    unsigned long fileLen;

    file = fopen(bstr_fname, "rb");
    if (!file) {
        fprintf(stderr, "Unable to open file %s\n", bstr_fname);
        return;
    }

    printf("Opened file %s\n", bstr_fname);

    // Get file length
    fseek(file, 0, SEEK_END);
    fileLen = ftell(file);
    fseek(file, 0, SEEK_SET);

    printf("File length: %lu\n", fileLen);

    // Allocate memory
    buffer = (char *)malloc(fileLen + 1);
    if (!buffer) {
        fprintf(stderr, "Memory error!");
        fclose(file);
        return;
    }

    // Read file contents into buffer
    fread(buffer, fileLen, 1, file);
    fclose(file);

    // Open ice_v device and write to it the buffer
    int ice_v_fd = open(ice40_dev, O_WRONLY);
    if (ice_v_fd < 0) {
        fprintf(stderr, "ERROR: Failed to open %s\n", ice40_dev);
        free(buffer);
        return -1;
    }

    ret = write(ice_v_fd, buffer, fileLen);

    if (ret < 0) {
        fprintf(stderr, "ERROR: Failed to write to %s\n", ice40_dev);
        close(ice_v_fd);
        free(buffer);
        return -1;
    } else {
        printf("Wrote %d bytes to %s\n", ret, ice40_dev);
    }

    free(buffer);
    close(ice_v_fd);

    return 0;
}
