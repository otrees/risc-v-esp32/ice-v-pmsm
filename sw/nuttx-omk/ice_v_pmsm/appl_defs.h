#ifndef _TEST_LPC_H
#define _TEST_LPC_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include "appl_config.h"

#ifdef CONFIG_APP_ICE_V_PMSM_WITH_SUITK
#define APPL_WITH_SUITK
#endif /*CONFIG_APP_ICE_V_PMSM_WITH_SUITK*/

#ifdef CONFIG_APP_ICE_V_PMSM_WITH_ZYNQ_DRV
#define APPL_WITH_ZYNQ_DRV
#endif /*CONFIG_APP_ICE_V_PMSM_WITH_ZYNQ_DRV*/

#ifdef CONFIG_APP_ICE_V_PMSM_SETUP_CLKOUT
#define APPL_RPI_PMSM_SETUP_CLKOUT
#endif /*CONFIG_APP_ICE_V_PMSM_SETUP_CLKOUT*/

#include <stdint.h>

/*
#define APPL_RUN_AT_MAIN_LOOP do { \
   pxmc_coordmv_process(); \
 } while(0)
*/

int pxmc_initialize(void);

int pxmc_done(void);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _TEST_LPC_H */

