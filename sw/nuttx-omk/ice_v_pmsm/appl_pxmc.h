/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  appl_pxmc.h - SPI connected motor control board specific
                extensions

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _APPL_PXMC_H_
#define _APPL_PXMC_H_

#include <stdint.h>
#include <pxmc.h>

struct spimc_state_t;
struct z3pmdrv1_state_t;

typedef struct pxmc_spimc_state_t {
  pxmc_state_t base;
 #ifdef APPL_WITH_ZYNQ_DRV
  struct z3pmdrv1_state_t *spimc_state;
 #else
  struct spimc_state_t *spimc_state;
 #endif
  uint32_t steps_pos_prev;
  uint32_t cur_d_cum_prev;
  uint32_t cur_q_cum_prev;
  int32_t  cur_d_err_sum;
  int32_t  cur_q_err_sum;
  short    cur_d_p;
  short    cur_d_i;
  short    cur_q_p;
  short    cur_q_i;
  short    cur_hold;
} pxmc_spimc_state_t;

#define pxmc_spimc_state_offs(_fld) \
                (((size_t)&((pxmc_spimc_state_t *)0L)->_fld) - \
                 ((size_t)&((pxmc_spimc_state_t *)0L)->base))

extern int appl_errstop_mode;
extern int appl_idlerel_time;
int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits);

int pxmc_spimc_pwm_direct_wr(unsigned chan, unsigned pwm, int en);

int pxmc_spimc_wait_rx_done(void);

long pxmc_get_slack_max(void);
void pxmc_reset_slack_max(void);

#endif /*_APPL_PXMC_H_*/
