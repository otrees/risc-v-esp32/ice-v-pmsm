/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmc.c - SPI connected motor control board specific
                extensionsposition

  Copyright (C) 2001-2015 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2015 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include <pxmc_gen_info.h>
#include <pxmc_dq_trans.h>
#include <pxmc_sin_fixed.h>
#include <stdlib.h>
#include <string.h>

#if !defined(clzl) && !defined(HAVE_CLZL)
#define clzl __builtin_clzl
#endif

#include "appl_defs.h"
#include "appl_pxmc.h"
#include "appl_utils.h"

#ifdef APPL_WITH_ZYNQ_DRV
#include "zynq_3pmdrv1_mc.h"
typedef z3pmdrv1_state_t spimc_state_t;
#define SPIMC_PWM_ENABLE   Z3PMDRV1_PWM_ENABLE
#define SPIMC_PWM_SHUTDOWN Z3PMDRV1_PWM_SHUTDOWN
#define SPIMC_CHAN_COUNT   Z3PMDRV1_CHAN_COUNT
#define spimc_transfer     z3pmdrv1_transfer
#define spimc_init         z3pmdrv1_init
#else
#include "pxmc_spimc.h"
#endif

pthread_t pxmc_base_thread_id;

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                           unsigned long index_irc, int diff2err);

#define PXML_MAIN_CNT 1

#undef PXMC_WITH_PT_ZIC
#define PXMC_PT_ZIC_MASK 0x8000

#define HAL_ERR_SENSITIVITY 20
#define HAL_ERR_MAX_COUNT    5

#define PXMC_LXPWR_PWM_CYCLE 2048

unsigned pxmc_spimc_pwm_magnitude = PXMC_LXPWR_PWM_CYCLE;

unsigned pxmc_spimc_mark_filt[PXML_MAIN_CNT];
unsigned pxmc_spimc_lxpwr_chips = 0;
int appl_errstop_mode = 0;

static inline
pxmc_spimc_state_t *pxmc_state2spimc_state(pxmc_state_t *mcs)
{
  pxmc_spimc_state_t *mcsrc;
 #ifdef UL_CONTAINEROF
  mcsrc = UL_CONTAINEROF(mcs, pxmc_spimc_state_t, base);
 #else /*UL_CONTAINEROF*/
  mcsrc = (pxmc_spimc_state_t*)((char*)mcs - __builtin_offsetof(pxmc_spimc_state_t, base));
 #endif /*UL_CONTAINEROF*/
  return mcsrc;
}

const uint8_t onesin10bits[1024]={
  0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,6,7,7,8,7,8,8,9,7,8,8,9,8,9,9,10
};

int
pxmc_inp_spimc_inp(struct pxmc_state *mcs)
{
  pxmc_spimc_state_t *mcsrc = pxmc_state2spimc_state(mcs);
  int chan=mcs->pxms_inp_info;
  long irc;
  long pos;

  /* read position from hardware */
  irc = mcsrc->spimc_state->act_pos;
  irc += mcsrc->spimc_state->pos_offset;
  pos = irc << PXMC_SUBDIV(mcs);
  mcs->pxms_as = pos - mcs->pxms_ap;
  mcs->pxms_ap = pos;

  /* Running of the motor commutator */
  if (mcs->pxms_flg & PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs, irc);

  return 0;
}

int
pxmc_inp_spimc_ap2hw(struct pxmc_state *mcs)
{
  pxmc_spimc_state_t *mcsrc = pxmc_state2spimc_state(mcs);
  int chan=mcs->pxms_inp_info;
  long irc;
  long pos_diff;

  irc = mcsrc->spimc_state->act_pos;
  pos_diff = mcs->pxms_ap - (irc << PXMC_SUBDIV(mcs));

  irc = pos_diff >> PXMC_SUBDIV(mcs);

  /* Adjust phase table alignemt to modified IRC readout  */
  mcs->pxms_ptofs += irc - mcsrc->spimc_state->pos_offset;

  mcsrc->spimc_state->pos_offset = irc;
  return 0;
}

inline unsigned
pxmc_spimc_bldc_hal_rd(pxmc_state_t *mcs)
{
  pxmc_spimc_state_t *mcsrc = pxmc_state2spimc_state(mcs);
  unsigned h = 0;
  int chan = mcs->pxms_out_info;

  h  = mcsrc->spimc_state->hal_sensors;

  /* return 3 bits corresponding to the HAL senzor input */
  return h;
}

#if 1
const unsigned char pxmc_lpc_bdc_hal_pos_table[8] =
{
  [0] = 0xff,
  [7] = 0xff,
  [1] = 0, /*0*/
  [5] = 1, /*1*/
  [4] = 2, /*2*/
  [6] = 3, /*3*/
  [2] = 4, /*4*/
  [3] = 5, /*5*/
};
#else
const unsigned char pxmc_lpc_bdc_hal_pos_table[8] =
{
  [0] = 0xff,
  [7] = 0xff,
  [1] = 0, /*0*/
  [5] = 5, /*1*/
  [4] = 4, /*2*/
  [6] = 3, /*3*/
  [2] = 2, /*4*/
  [3] = 1, /*5*/
};
#endif

int pxmc_spimc_pwm_direct_wr(unsigned chan, unsigned pwm, int en)
{

  return 0;
}

/**
 * pxmc_spimc_pwm3ph_wr - Output of the 3-phase PWM to the hardware
 * @mcs:  Motion controller state information
 */
/*static*/ inline void
pxmc_spimc_pwm3ph_wr(pxmc_state_t *mcs, uint32_t pwm1, uint32_t pwm2, uint32_t pwm3)
{
  pxmc_spimc_state_t *mcsrc = pxmc_state2spimc_state(mcs);
  int chan = mcs->pxms_out_info;

  mcsrc->spimc_state->pwm[0] = pwm1;
  mcsrc->spimc_state->pwm[1] = pwm2;
  mcsrc->spimc_state->pwm[2] = pwm3;
}

static inline void
pxmc_spimc_process_hal_error(struct pxmc_state *mcs)
{
  if (mcs->pxms_halerc >= HAL_ERR_SENSITIVITY * HAL_ERR_MAX_COUNT)
  {
    pxmc_set_errno(mcs, PXMS_E_HAL);
    mcs->pxms_ene = 0;
    mcs->pxms_halerc--;
  }
  else
    mcs->pxms_halerc += HAL_ERR_SENSITIVITY;
}

int
pxmc_inp_spimc_ptofs_from_index_poll(struct pxmc_state *mcs, int diff2err)
{
  pxmc_spimc_state_t *mcsrc = pxmc_state2spimc_state(mcs);
  int chan=mcs->pxms_inp_info;
  long irc;
  long index_irc;

  if (1)
    return 0;

  irc = mcsrc->spimc_state->act_pos + mcsrc->spimc_state->pos_offset;
  index_irc = mcsrc->spimc_state->index_pos + mcsrc->spimc_state->pos_offset;

  return pxmc_ptofs_from_index(mcs, irc, index_irc, diff2err);
}

/**
 * pxmc_spimc_pwm3ph_out - Phase output for brush-less 3-phase motor
 * @mcs:  Motion controller state information
 */
int
pxmc_spimc_pwm3ph_out(pxmc_state_t *mcs)
{
  pxmc_spimc_state_t *mcsrc = pxmc_state2spimc_state(mcs);
  typeof(mcs->pxms_ptvang) ptvang;
  int sync_mode = 0;
  unsigned char hal_pos;
  uint32_t pta;
  int32_t  sin_val, cos_val;
  int32_t  pwm_d,   pwm_q;
  int32_t  pwm_alp, pwm_bet;
  uint32_t pwm1, pwm2, pwm3;
  short ene;
  int wind_current[4];

  if (!(mcs->pxms_flg & PXMS_PTI_m) || !(mcs->pxms_flg & PXMS_PHA_m) ||
      (mcs->pxms_flg & PXMS_PRA_m))
  {
    short ptindx;
    short ptirc = mcs->pxms_ptirc;
    short divisor = mcs->pxms_ptper * 6;

    hal_pos = pxmc_lpc_bdc_hal_pos_table[pxmc_spimc_bldc_hal_rd(mcs)];

    if (hal_pos == 0xff)
    {
      if (mcs->pxms_ene)
        pxmc_spimc_process_hal_error(mcs);
    }
    else
    {
      if (mcs->pxms_halerc)
        mcs->pxms_halerc--;

      ptindx = (hal_pos * ptirc + divisor / 2) / divisor;

      if (!(mcs->pxms_flg & PXMS_PTI_m) || (mcs->pxms_flg & PXMS_PRA_m))
      {
        if (((hal_pos != mcs->pxms_hal) && (mcs->pxms_hal != 0x40)) && 1)
        {
          short ptindx_prev = (mcs->pxms_hal * ptirc + divisor / 2) / divisor;;

          if ((ptindx > ptindx_prev + ptirc / 2) ||
              (ptindx_prev > ptindx + ptirc / 2))
          {
            ptindx = (ptindx_prev + ptindx - ptirc) / 2;

            if (ptindx < 0)
              ptindx += ptirc;
          }
          else
          {
            ptindx = (ptindx_prev + ptindx) / 2;
          }

          mcs->pxms_ptindx = ptindx;

          mcs->pxms_ptofs = (mcs->pxms_ap >> PXMC_SUBDIV(mcs)) + mcs->pxms_ptshift - ptindx;

          pxmc_set_flag(mcs, PXMS_PTI_b);
          pxmc_clear_flag(mcs, PXMS_PRA_b);
        }
        else
        {
          if (!(mcs->pxms_flg & PXMS_PTI_m))
            mcs->pxms_ptindx = ptindx;
        }
      } else {
        /* if phase table position to mask is know do fine phase table alignment */
        if (mcs->pxms_cfg & PXMS_CFG_I2PT_m) {
          int res;
          res = pxmc_inp_spimc_ptofs_from_index_poll(mcs, 0);
          if (res < 0) {
            pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
          } else if (res) {
            pxmc_set_flag(mcs, PXMS_PTI_b);
            pxmc_set_flag(mcs, PXMS_PHA_b);
          }
        }
      }
      mcs->pxms_hal = hal_pos;
    }
  }

  {
    /*wind_current[0]=(ADC->ADDR0 & 0xFFF0)>>4;*/
    /* FIXME - check winding current against limit */
    /* pxmc_set_errno(mcs, PXMS_E_WINDCURRENT); */
  }

  if (!sync_mode) {
    ptvang = mcs->pxms_ptvang;
    pwm_d = 0;
    pwm_q = mcs->pxms_ene;
  } else {
    ptvang = 0;
    pwm_d = mcs->pxms_flg & PXMS_BSY_m? mcs->pxms_me: 0;
    pwm_q = 0;
  }

  if (pwm_d || pwm_q) {
    int32_t indx;
    uint32_t ptscale_mult = mcs->pxms_ptscale_mult |
                  (mcs->pxms_ptscale_shift << 16);

    indx = mcs->pxms_ptindx;

    if (indx < 0)
      pta = indx + mcs->pxms_ptirc;
    else
      pta = indx;

    pta *= ptscale_mult;

    pta -= PXMC_SIN_FIX_2PI3;

    pxmc_sincos_fixed_inline(&sin_val, &cos_val, pta, 16);
    pxmc_dq2alpbet(&pwm_alp, &pwm_bet, pwm_d, pwm_q, sin_val, cos_val);
    pxmc_alpbet2pwm3ph(&pwm1, &pwm2, &pwm3, pwm_alp, pwm_bet);

    /*printf("pwm d %4d q %4d alp %4d bet %4d a %4d b %4d c %4d\n",
           pwm_d, pwm_q, pwm_alp >> 16, pwm_bet >> 16, pwm1, pwm2, pwm3);*/


#ifdef PXMC_WITH_PT_ZIC
    if (labs(mcs->pxms_as) < (10 << PXMC_SUBDIV(mcs)))
    {
    }
#endif /*PXMC_WITH_PT_ZIC*/

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    {
      unsigned long pwm_mag = pxmc_spimc_pwm_magnitude;
      if (0)
        pwm1 = SPIMC_PWM_SHUTDOWN;
      else
        pwm1 = (((unsigned long long)pwm1 * pwm_mag) >> 15) | SPIMC_PWM_ENABLE;
      if (0)
        pwm2 = SPIMC_PWM_SHUTDOWN;
      else
        pwm2 = (((unsigned long long)pwm2 * pwm_mag) >> 15) | SPIMC_PWM_ENABLE;
      if (0)
        pwm3 = SPIMC_PWM_SHUTDOWN;
      else
        pwm3 = (((unsigned long long)pwm3 * pwm_mag) >> 15) | SPIMC_PWM_ENABLE;
    }
    pxmc_spimc_pwm3ph_wr(mcs, pwm1, pwm2, pwm3);

  }
  else
  {
    pxmc_spimc_pwm3ph_wr(mcs, 0, 0, 0);
  }

  /*printf("hal_pos %d pwm %4d %4d %4d\n", hal_pos, pwm1 & 0xffff,
                                  pwm2 & 0xffff, pwm3  & 0xffff);*/

  return 0;
}

/**
 * pxmc_spimc_pwm_dc_out - DC motor CW and CCW PWM output
 * @mcs:  Motion controller state information
 */
int
pxmc_spimc_pwm_dc_out(pxmc_state_t *mcs)
{
  int chan = mcs->pxms_out_info;
  int ene = mcs->pxms_ene;

  if (ene < 0) {
    ene = -ene;
    if (ene > 0x7fff)
      ene = 0x7fff;
    ene = (ene * (pxmc_spimc_pwm_magnitude + 5)) >> 15;
    pxmc_spimc_pwm3ph_wr(mcs, SPIMC_PWM_ENABLE, ene | SPIMC_PWM_ENABLE, 0);
  } else {
    if (ene > 0x7fff)
      ene = 0x7fff;
    ene = (ene * (pxmc_spimc_pwm_magnitude + 5)) >> 15;
    pxmc_spimc_pwm3ph_wr(mcs, ene | SPIMC_PWM_ENABLE, SPIMC_PWM_ENABLE, 0);
  }

  return 0;
}

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                           unsigned long index_irc, int diff2err)
{
  long ofsl;
  short ofs;

  ofs = ofsl = index_irc - mcs->pxms_ptmark;

  if (diff2err) {
    short diff;
    diff = (unsigned short)ofs - (unsigned short)mcs->pxms_ptofs;
    if (diff >= mcs->pxms_ptirc / 2)
      diff -= mcs->pxms_ptirc;
    if (diff <= -mcs->pxms_ptirc / 2)
      diff += mcs->pxms_ptirc;
    if (diff < 0)
      diff = -diff;
    if(diff >= mcs->pxms_ptirc / 6) {
      return -1;
    }
  } else {
    long diff;
    diff = (unsigned long)ofsl - irc;
    ofs = ofsl - (diff / mcs->pxms_ptirc) * mcs->pxms_ptirc;
  }

  mcs->pxms_ptofs = ofs;

  return 1;
}

/**
 * pxmc_dummy_con - Dummy controller for synchronous BLDC/PMSM/steper drive
 * @mcs:        Motion controller state information
 */
int
pxmc_dummy_con(pxmc_state_t *mcs)
{
  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return NULL;
}

int pxmc_fill_ptscale_for_sin_fixed(pxmc_state_t *mcs)
{
  unsigned long ptirc = mcs->pxms_ptirc;
  unsigned long ptper = mcs->pxms_ptper;
  unsigned long scale_mult;
  unsigned long scale_shift;
  int shift;

  shift = clzl(ptper);
  ptper <<= shift;

  scale_mult = (ptper + ptirc / 2) / ptirc;
  if (shift < 32)
    scale_mult <<= 32 - shift;
  if (shift > 32)
    scale_mult >>= shift - 32;

  mcs->pxms_ptscale_mult = scale_mult & 0xffff;
  mcs->pxms_ptscale_shift = scale_mult >> 16;

  return 0;
}


spimc_state_t spimc_state0 = {
 #ifndef APPL_WITH_ZYNQ_DRV
 //.spi_dev = "/dev/spidev0.1",
 //.spi_dev = "/dev/spidev1.0",
 .spi_dev = "/dev/spi2",
 #endif /*APPL_WITH_ZYNQ_DRV*/
};

pxmc_spimc_state_t mcs0 =
{
.base = {
.pxms_flg =
  PXMS_ENI_m,
.pxms_do_inp =
  pxmc_inp_spimc_inp,
.pxms_do_con =
  pxmc_pid_con /*pxmc_dummy_con*/,
.pxms_do_out =
  pxmc_spimc_pwm3ph_out /*pxmc_spimc_pwm_dc_out*/,
  .pxms_do_deb = 0,
  .pxms_do_gen = 0,
.pxms_do_ap2hw =
  pxmc_inp_spimc_ap2hw,
  .pxms_ap = 0, .pxms_as = 0,
  .pxms_rp = 55 * 256, .pxms_rs = 0,
 #ifndef PXMC_WITH_FIXED_SUBDIV
  .pxms_subdiv = 8,
 #endif /*PXMC_WITH_FIXED_SUBDIV*/
  .pxms_md = 800 << 8, .pxms_ms = 500, .pxms_ma = 10,
  .pxms_inp_info = 0,
  .pxms_out_info = 0,
  .pxms_ene = 0, .pxms_erc = 0,
  .pxms_p = 20, .pxms_i = 10, .pxms_d = 0, .pxms_s1 = 0, .pxms_s2 = 0,
  .pxms_me = 0x7e00/*0x7fff*/,
.pxms_cfg =
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
  PXMS_CFG_I2PT_m * 0 | 0x2,

  .pxms_ptper = 1,
  .pxms_ptirc = 1000,
  .pxms_ptmark = 1180,
  /*.pxms_ptamp = 0x7fff,*/

  .pxms_hal = 0x40,
},
  .spimc_state = &spimc_state0,
  .cur_d_p = 150,
  .cur_d_i = 6000,
  .cur_q_p = 150,
  .cur_q_i = 6000,
  .cur_hold = 200,
};


pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT] = {&mcs0.base};


pxmc_state_list_t  pxmc_main_list =
{
.pxml_arr =
  pxmc_main_arr,
  .pxml_cnt = 0
};


static inline void pxmc_sfi_input(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENI_m - check if input (IRC) update is enabled */
    if (mcs->pxms_flg & PXMS_ENI_m)
    {
      pxmc_call(mcs, mcs->pxms_do_inp);
    }
  }
}

static inline void pxmc_sfi_controller_and_output(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENR_m - check if controller is enabled */
    if (mcs->pxms_flg & PXMS_ENR_m || mcs->pxms_flg & PXMS_ENO_m)
    {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg & PXMS_ENR_m)
      {

        pxmc_call(mcs, mcs->pxms_do_con);

        /* PXMS_ERR_m - if axis in error state */
        if (mcs->pxms_flg & PXMS_ERR_m)
          mcs->pxms_ene = 0;
      }

      /* for bushless motors, it is necessary to call do_out
        even if the controller is not enabled and PWM should be provided. */
      pxmc_call(mcs, mcs->pxms_do_out);
    }
  }
}

static inline void pxmc_sfi_generator(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENG_m - check if requested value (position) generator is enabled */
    if (mcs->pxms_flg & PXMS_ENG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_gen);
    }
  }
}

static inline void pxmc_sfi_dbg(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    if (mcs->pxms_flg & PXMS_DBG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_deb);
    }
  }
}

static inline void pxmc_spimc_rq_transfer(int flags)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_spimc_state_t *mcsrc;

  pxmc_for_each_mcs(var, mcs)
  {
    mcsrc = pxmc_state2spimc_state(mcs);
    spimc_transfer(mcsrc->spimc_state, flags);
  }
}

void pxmc_samplig_period(void)
{
  pxmc_sfi_input();
  pxmc_sfi_controller_and_output();
  pxmc_spimc_rq_transfer(1);
  pxmc_sfi_generator();
  pxmc_sfi_dbg();

  do_pxmc_coordmv();
}

sample_period_t pxmc_sample_period;
long pxmc_slack_max = 0;

void *pxmc_base_thread(void *arg)
{
  /* Set initial sampling frequnecy to 1 kHz (1000000 nsec) */
  sample_period_setup(&pxmc_sample_period, 1000 * 1000);

  do {
    long slack;
    pxmc_samplig_period();
    slack = sample_period_wait_next(&pxmc_sample_period);
    if (slack > pxmc_slack_max)
      pxmc_slack_max = slack;
  } while(1);
}

#ifdef WITH_SFI_SEL
/**
 * pxmc_get_sfi_hz - Reads sampling frequency of axis
 * @mcs:        Motion controller state information
 */
long pxmc_get_sfi_hz(pxmc_state_t *mcs)
{
  unsigned long nsec;
  unsigned long nsec_in_sec = 1000 * 1000 * 1000;
  nsec = sample_period_get(&pxmc_sample_period);
  return  (nsec_in_sec + nsec / 2) / nsec;
}

long pxmc_get_slack_max(void)
{
  return pxmc_slack_max;
}

void pxmc_reset_slack_max(void)
{
  pxmc_slack_max = 0;
}

/**
 * pxmc_sfi_sel - Setting sampling frequency of PXMC subsystem (TPU_TGR4A)
 * @sfi_hz:     Requested sampling frequency in Hz
 *
 * Function returns newly selected sampling frequency
 * after rounding or -1 if there is problem to set
 * requested frequency.
 */
long pxmc_sfi_sel(long sfi_hz)
{
  unsigned long nsec_in_sec = 1000 * 1000 * 1000;
  unsigned long nsec;

  if((sfi_hz < 500) || (sfi_hz > 5 * 1000))
    return -1;

  nsec = (nsec_in_sec + sfi_hz / 2) / sfi_hz;
  sample_period_modify(&pxmc_sample_period, nsec);
  return  (nsec_in_sec + nsec / 2) / nsec;
}
#endif /* WITH_SFI_SEL */

int pxmc_clear_power_stop(void)
{
  return 0;
}

int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits)
{
  unsigned short flg;
  unsigned short chan;
  unsigned long busy_bits = 0;
  unsigned long error_bits = 0;
  pxmc_state_t *mcs;
  flg=0;
  pxmc_for_each_mcs(chan, mcs) {
    if(mcs) {
      flg |= mcs->pxms_flg;
      if (mcs->pxms_flg & PXMS_BSY_m)
        busy_bits |= 1 << chan;
      if (mcs->pxms_flg & PXMS_ERR_m)
        error_bits |= 1 << chan;
    }
  }
  if (appl_errstop_mode) {
    if((flg & PXMS_ENG_m) && (flg & PXMS_ERR_m)) {
      pxmc_for_each_mcs(chan, mcs) {
        if(mcs&&(mcs->pxms_flg & PXMS_ENG_m)) {
          pxmc_stop(mcs, 0);
        }
      }
    }
  }

  if (pbusy_bits != NULL)
    *pbusy_bits = busy_bits;
  if (perror_bits != NULL)
    *perror_bits = error_bits;

  return flg;
}

int pxmc_axis_rdmode(pxmc_state_t *mcs)
{
  if (mcs->pxms_do_out == pxmc_spimc_pwm_dc_out)
    return PXMC_AXIS_MODE_DC;
  if (mcs->pxms_do_out == pxmc_spimc_pwm3ph_out)
    return PXMC_AXIS_MODE_BLDC;
  return -1;
}

/**
 * pxmc_axis_mode - Sets axis mode.[extern API]
 * @mcs:        Motion controller state information
 * @mode:       0 .. previous mode, 1 .. stepper motor mode,
 *              2 .. stepper motor with IRC feedback and PWM ,
 *              3 .. stepper motor with PWM control
 *              4 .. DC motor with IRC feedback and PWM
 *
 */
int
pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
  int res = 0;
  int prev_mode;

  pxmc_axis_release(mcs);
  pxmc_clear_flag(mcs, PXMS_ENI_b);
  pxmc_clear_flag(mcs,PXMS_ENO_b);
  /*TODO Clear possible stall index flags from hardware */

  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);

  prev_mode = pxmc_axis_rdmode(mcs);

  if (mode == PXMC_AXIS_MODE_NOCHANGE)
    mode = prev_mode;
  if (mode < 0)
    return -1;
  if (!mode)
    mode = PXMC_AXIS_MODE_DC;

  switch (mode) {
    case PXMC_AXIS_MODE_DC:
      mcs->pxms_do_out = pxmc_spimc_pwm_dc_out;
      break;
    case PXMC_AXIS_MODE_BLDC:
      mcs->pxms_do_out = pxmc_spimc_pwm3ph_out;
      pxmc_fill_ptscale_for_sin_fixed(mcs);
      break;
    default:
      return -1;
  }

  /*TODO Clear possible stall index flags from hardware */

  /* Request new phases alignment for changed parameters */
  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);
  pxmc_set_flag(mcs, PXMS_ENI_b);
  return res;
}

int pxmc_done(void)
{
  int var;
  pxmc_state_t *mcs;

  if (!pxmc_main_list.pxml_cnt)
    return 0;

  pxmc_for_each_mcs(var, mcs)
  {
    pxmc_axis_release(mcs);
  }

  pxmc_main_list.pxml_cnt = 0;
  __memory_barrier();

  return 0;
}

int pxmc_initialize(void)
{
  int res;

  pxmc_main_list.pxml_cnt = 0;
  pxmc_dbg_hist = NULL;

  if (spimc_init(mcs0.spimc_state) < 0)
    return -1;

  pxmc_fill_ptscale_for_sin_fixed(pxmc_main_list.pxml_arr[0]);

  __memory_barrier();
  pxmc_main_list.pxml_cnt = PXML_MAIN_CNT;

  if (create_rt_task(&pxmc_base_thread_id, 200, pxmc_base_thread, NULL) < 0)
    return -1;

  return 0;
}
