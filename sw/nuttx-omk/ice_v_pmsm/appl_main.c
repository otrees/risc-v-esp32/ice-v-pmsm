#include <system_def.h>
#include <stdio.h>
#include <string.h>

#include "appl_defs.h"
#include "appl_utils.h"

void appl_stop(void)
{
  pxmc_done();
  fprintf(stderr, "Application abnormal termination\n");
  sleep(1);
  /* stop clock pin driving FPGA to ensure failase state */
}

/***********************************/
int main(int argc, char *argv[])
{
  appl_setup_environment(argv[0]);

  pxmc_initialize();

  do {
    cmdproc_poll();
  } while(1);

  return 0;
}

