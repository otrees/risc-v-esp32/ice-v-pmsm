#include <system_def.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cmd_proc.h>

#include "appl_defs.h"

typedef struct cmdproc_ready_check_state_t {
  unsigned long axes_rq_mask;
  int global_rq;
  cmd_io_t* peer_cmd_io;
} cmdproc_ready_check_state_t;

extern cmd_io_t cmd_io_std_line;

cmdproc_ready_check_state_t cmdproc_ready_check_state_std_line = {
  .peer_cmd_io = &cmd_io_std_line,
};

cmdproc_ready_check_state_t *const cmdproc_ready_check_state_arr[] = {
  &cmdproc_ready_check_state_std_line,
  NULL
};

extern cmd_des_t const *cmd_pxmc_base[];
extern cmd_des_t const *cmd_appl_tests[];
extern cmd_des_t const *cmd_pxmc_ptable[];
extern cmd_des_t const *cmd_pxmc_coordmv[];
extern cmd_des_t const *cmd_appl_specific[];
extern cmd_des_t const *cmd_appl_pxmc[];

extern cmd_des_t const cmd_des_dprint;

cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help =
{
  0, 0,
  "help", "prints help for commands",
  cmd_do_help,
  {
    (char *) &cmd_list
  }
};

cmdproc_ready_check_state_t *cmdproc_ready_state4cmd_io(cmd_io_t *cmd_io)
{
  cmdproc_ready_check_state_t * const *ppckst = cmdproc_ready_check_state_arr;
  while (*ppckst) {
    if ((*ppckst)->peer_cmd_io == cmd_io)
      return *ppckst;
    ppckst++;
  }
  return NULL;
}

int cmd_do_r_one(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned chan;
  unsigned long val;
  cmdproc_ready_check_state_t *ckst;

  if (*param[2] != ':') return -CMDERR_OPCHAR;
  chan = *param[1] - 'A';
  if(chan >= 31) return -CMDERR_BADREG;

  ckst = cmdproc_ready_state4cmd_io(cmd_io);
  if (ckst == NULL)
    return -CMDERR_BADDIO;

  ckst->axes_rq_mask |= 1l << chan;
  return 0;
}

int cmd_do_r_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  cmdproc_ready_check_state_t *ckst;

  if(*param[2] != ':') return -CMDERR_OPCHAR;

  ckst = cmdproc_ready_state4cmd_io(cmd_io);
  if (ckst == NULL)
    return -CMDERR_BADDIO;

  ckst->global_rq = 1;
  return 0;
}

cmd_des_t const cmd_des_r_one={0, CDESM_OPCHR,
			"R?","send R?! or FAIL?! at axis finish",cmd_do_r_one,{}};

cmd_des_t const cmd_des_r_all={0, CDESM_OPCHR,
			"R","send R! or FAIL! at finish",cmd_do_r_all,{}};

cmd_des_t const *cmd_list_main[] =
{
  &cmd_des_help,
  //CMD_DES_INCLUDE_SUBLIST(cmd_appl_tests),
#ifdef CONFIG_PXMC
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_base),
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_ptable),
#endif
  &cmd_des_r_one,
  &cmd_des_r_all,
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_coordmv),
  //CMD_DES_INCLUDE_SUBLIST(cmd_appl_specific),
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_pxmc),
  //&cmd_des_dprint,
  NULL
};

cmd_des_t const **cmd_list = cmd_list_main;

int cmdproc_process_ready(cmd_io_t* cmd_io,
                  cmdproc_ready_check_state_t *ckst,
                  unsigned long busy_bits, unsigned long error_bits)
{
  unsigned int chan;
  unsigned long send_bits = ckst->axes_rq_mask &
                            (error_bits | ~busy_bits);
  int send_global = ckst->global_rq && (error_bits || !busy_bits);

  if (cmd_io->priv.ed_line.out->inbuf || (!send_bits && !send_global))
    return 0;

  if (send_bits) {
    chan = ffsl(send_bits) - 1;
    ckst->axes_rq_mask &= ~(1l << chan);
    if (error_bits & (1l << chan))
      cmd_io_write(cmd_io,"FAIL",4);
    else
      cmd_io_write(cmd_io,"R",1);
    cmd_io_putc(cmd_io,chan+'A');
    cmd_io_putc(cmd_io,'!');
    cmd_io_putc(cmd_io,'\r');
    cmd_io_putc(cmd_io,'\n');
    return 1;
  }

  ckst->global_rq = 0;
  if (error_bits)
    cmd_io_write(cmd_io,"FAIL",4);
  else
    cmd_io_write(cmd_io,"R",1);
  cmd_io_putc(cmd_io,'!');
  cmd_io_putc(cmd_io,'\r');
  cmd_io_putc(cmd_io,'\n');
  return 1;
}

int cmdproc_poll(void)
{
/*  static typeof(actual_msec) dbg_prt_last_msec;
  static typeof(actual_msec) old_check_time;
  typeof(actual_msec) new_check_time;
*/
  int ret = 0;
  unsigned long busy_bits, error_bits;

/*
  lt_mstime_update();

  new_check_time = actual_msec;
  if (new_check_time != old_check_time) {
    cmdproc_ready_check_state_t * const *ppckst;
    old_check_time = new_check_time;
    pxmc_process_state_check(&busy_bits, &error_bits);
    for (ppckst = cmdproc_ready_check_state_arr; *ppckst; ppckst++)
      ret |= cmdproc_process_ready((*ppckst)->peer_cmd_io, *ppckst,
                                   busy_bits, error_bits);
  }
*/

  ret |= cmd_processor_run(&cmd_io_std_line, cmd_list_main);

  return ret;
}
