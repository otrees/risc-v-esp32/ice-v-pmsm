#ifndef _GENPOSIX_CPU_DEF_H
#define _GENPOSIX_CPU_DEF_H

#include <stdatomic.h>

#ifndef CODE
  #define CODE
#endif

#ifndef XDATA
  #define XDATA
#endif

#ifndef DATA
  #define DATA
#endif

#define test_bit(nr,v)    (atomic_fetch_or_explicit(v,0)&(1<<(nr), memory_order_seq_cst))
#define set_bit(nr,v)    (atomic_fetch_or_explicit(v,(1<<(nr)), memory_order_seq_cst))
#define clear_bit(nr,v)  (atomic_fetch_and_explicit(v,~(1<<(nr)), memory_order_seq_cst))
#define test_and_set_bit(nr,v) \
  ({ typeof(*(v)) __bitmsk = 1<<(nr); \
     atomic_fetch_or_explicit(v,__bitmsk, memory_order_seq_cst)&__bitmsk?1:0; \
  })

#define atomic_clear_mask_b1(mask, v) atomic_fetch_and_explicit(v,~(mask), memory_order_seq_cst)
#define atomic_set_mask_b1(mask, v)   atomic_fetch_or_explicit(v,(mask), memory_order_seq_cst)

#define atomic_clear_mask_w1(mask, v) atomic_fetch_and_explicit(v,~(mask), memory_order_seq_cst)
#define atomic_set_mask_w1(mask, v)   atomic_fetch_or_explicit(v,(mask), memory_order_seq_cst)

#define atomic_clear_mask_w(mask, v)  atomic_fetch_and_explicit(v,~(mask), memory_order_seq_cst)
#define atomic_set_mask_w(mask, v)    atomic_fetch_or_explicit(v,(mask), memory_order_seq_cst)

#define atomic_clear_mask(mask, v)    atomic_fetch_and_explicit(v,~(mask), memory_order_seq_cst)
#define atomic_set_mask(mask, v)      atomic_fetch_or_explicit(v,(mask), memory_order_seq_cst)

/* Arithmetic functions */
#define sat_add_slsl(__x,__y)                                   \
        do {                                                    \
                typeof(__x) tmp;                                \
                tmp = (__x) + (__y);                            \
                if ((__x) > 0 && (__y) > 0 && tmp < 0)          \
                        tmp = +0x7fffffff;                      \
                else if ((__x) < 0 && (__y) < 0 && tmp >= 0)    \
                        tmp = -0x80000000;                      \
                (__x) = tmp;                                    \
        } while (0)

#ifndef __memory_barrier
#define __memory_barrier() \
__asm__ __volatile__("": : : "memory")
#endif

/*masked fields macros*/
#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

#define IRQ_HANDLER_FNC(M_fnc_name) \
   int M_fnc_name(int __irq_handler_irqidx, void *__irq_process_context, \
                  void *__irq_handler_context)

typedef IRQ_HANDLER_FNC(irq_handler_t);

#define irq_handler_get_irqidx() (__irq_handler_irqidx)
#define irq_handler_get_context() (__irq_handler_context)

#define IRQ_HANDLED 0

#define request_irq(M_irqnum, M_handler, M_flags, M_name, M_context) \
         (irq_attach((M_irqnum), (M_handler), (M_context)) == OK? \
          (up_enable_irq(M_irqnum), 0) : -1)
#define free_irq(M_irqnum, M_context) \
         do { up_disable_irq(M_irqnum); \
              irq_detach(M_irqnum); } while(0)

#define irq_handler_get_irqnum() 0

#endif /* _GENPOSIX_CPU_DEF_H */
