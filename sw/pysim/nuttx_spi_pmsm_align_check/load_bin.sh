#!/bin/bash -v

esptool.py -c esp32c3 elf2image -fs 4MB -fm dio -ff 40m -o start.bin nuttx_spi_pmsm_align_check
/opt/openocd/bin/openocd -c 'set ESP_RTOS none' -f board/esp32c3-builtin.cfg -c "program_esp start.bin 0x10000 verify reset exit"
