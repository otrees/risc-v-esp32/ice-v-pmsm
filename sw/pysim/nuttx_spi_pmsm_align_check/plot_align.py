import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
# set plot to be interactive
matplotlib.use('Qt5Agg')

# Read the data
data = pd.read_csv('log.dat', sep='\t', header=None, names=['time', 'input', 'irc', 'diff', 'idx_cnt'], index_col=False)

print(data)

fig, ax = plt.subplots(4, 1)

data = data[data['idx_cnt'] > 0]

ax[0].plot(data['time'].to_numpy(), data['input'].to_numpy(), label='input')
ax[1].plot(data['time'].to_numpy(), data['irc'].to_numpy(), label='irc')
ax[2].plot(data['time'].to_numpy(), data['diff'].to_numpy(), label='diff')
ax[3].plot(data['time'].to_numpy(), data['idx_cnt'].to_numpy(), label='idx_cnt')

print(data['diff'].mean())

plt.tight_layout()

plt.show()