import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
# set plot to be interactive
matplotlib.use('Qt5Agg')

# Read the data
data = pd.read_csv('curr_log.dat', sep='\t', header=None,
                   names=['time', 'input', 'real', 'action', 'curr_a', 'curr_b', 'curr_c'], index_col=False)

print(data)

fig, ax = plt.subplots(3, 1)
data['time'] = data['time'] - data['time'].iloc[0]
data = data[data['time'] < 5]
# data = data[data['time'] > 2]


ax[0].plot(data['time'].to_numpy(), data['input'].to_numpy(), label='input')
ax[0].plot(data['time'].to_numpy(), data['real'].to_numpy(), label='real')
ax[1].plot(data['time'].to_numpy(), data['action'].to_numpy(), label='diff')
ax[2].plot(data['time'].to_numpy(), data['curr_a'].to_numpy(), label='curr_a')
ax[2].plot(data['time'].to_numpy(), data['curr_b'].to_numpy(), label='curr_b')
ax[2].plot(data['time'].to_numpy(), data['curr_c'].to_numpy(), label='curr_c')

plt.tight_layout()

plt.show()