# Project Overview

The ice_v_pmsm repository contains an open-source project for controlling Permanent Magnet Synchronous Motors (PMSM) using the ICE-V Wireless board. This project integrates real-time motor control using the Lattice iCE40 FPGA and an ESP32-C3 microcontroller running NuttX RTOS. It works with the PXMC motion control library and pysimCoder for model-based control of PMSM motors.

The project was created and is more described in the [diploma thesis](http://hdl.handle.net/10467/117046 )

This repository provides both hardware and software necessary to run the motion control system.

## Hardware (hw) Directory

The HW directory contains the KiCad project files for the custom adapter board that connects the ICE-V Wireless board to the 3p-motor-driver-1 power stage. It is responsible for routing PWM signals and control inputs between the ICE-V Wireless board and the power stage, providing power for the ICE-V Wireless from the main power supply and allows access to the remaining GPIO pins from both the ESP32-C3 and iCE40 FPGA for future extensions. It also implements communication options with the board via CAN.

## Software (sw) Directory

The SW directory contains the software required for controlling the PMSM motor. It contains two parts, the "nuttx-omk" directory with the NuttX project and app with the PXMC library, and the "pysim" directory with models for PysimCoder. The following PysimCoder models are included:
1. Encoder Offset Calibration Model

    This model is designed to calibrate the encoder and hall sensor offset of the PMSM motor.

2. PMSM Motor Control Model

    This model implements position control of the PMSM motor.

3. PMSM Control with Current Feedback on D-axis

    This model extends the PMSM motor control model by adding current feedback on the D-axis, which controls the motor's magnetic flux. The current feedback is not fully tested and also the control frequency is not high engouh for good results.

## Build Dependencies

- riscv64-unknown-elf GCC and Binutils toolchain is used for NutttX and applications builds
- Espressif's [https://github.com/espressif/esptool](https://github.com/espressif/esptool) is used for ESP32C3 firmware images creation
- GHDL ([https://github.com/ghdl/ghdl](https://github.com/ghdl/ghdl)) and related Yosys plugin ([https://github.com/ghdl/ghdl-yosys-plugin](https://github.com/ghdl/ghdl-yosys-plugin)) are used for VHDL project build
- Yosys HQ with nextpnr and IceStorm are used for Lattice iCE-40 bitstream synthesis ([https://github.com/YosysHQ](https://github.com/YosysHQ))

## Build Instructions

The top level Makefile allows to build PMSM control peripherals VHDL sources for iCE-40 (`make hdl`). The built bitstream
can be distributed to applications by `make hdl-to-apps`. The NuttX operation system sources are cloned, configured
and build for ICE-V PMSM application and pysimCoder model by `make nuttx-build`. The OMK build system is used for
PMSM NuttX C code PXMC based application. The build is invoked by `make omk-apps`. The both
software related targets can be build by `make sw` and whole project by simple `make` or `make all`.

## Documentation, Publications and Links

- Janousek, J.: Open-source Motion Control on Mid-range and Small FPGAs; Master Thesis; Czech Technical University in Prague, 2024; [PDF](https://dspace.cvut.cz/bitstream/handle/10467/117046/F3-DP-2024-Janousek-Jakub-DP_Janousek.pdf)
- this project [ice-v-pmsm](https://gitlab.fel.cvut.cz/otrees/risc-v-esp32/ice-v-pmsm) repository at [Open Technologies Research Education and Exchange Services](https://gitlab.fel.cvut.cz/otrees/org/-/wikis/home) atd [CTU](https://www.cvut.cz/) [FEE](https://fel.cvut.cz/) GitLab
- ICE-V Wireless project site [https://github.com/ICE-V-Wireless](https://github.com/ICE-V-Wireless)
- pysimCoder - Block diagram editor and real time code generator for Python [https://github.com/robertobucher/pysimCoder](https://github.com/robertobucher/pysimCoder)
- PXMC - Portable, highly eXtendable Motion Control library [https://www.pxmc.org/](https://www.pxmc.org/)
