-- provides frequency division by 12
-- initialy intended to make 4.17Mhz from 50Mhz

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity div128 is

port (
	clk_in: in std_logic;
	rst: in std_logic;
	fail_safe: out std_logic
);
end div128;


architecture behavioral of div128 is
	signal count : std_logic_vector (6 downto 0);
	signal rst_prev: std_logic;
begin


	seq : process
	begin
		wait until (clk_in'event and clk_in='1');
		rst_prev <= rst;
		if rst='1' and rst_prev='0' then
			count <= "0000000";
			fail_safe <= '0';
		else
			count <= std_logic_vector(unsigned(count) + 1);
		end if;

		if count = "1111111" then
			fail_safe <= '1';
		else
			fail_safe <= '0';
		end if;
	end process;



end behavioral;
