--
-- * ICE-V (ESP32C3 + iCE40) BLDC/PMSM motor control design *
--
-- (c) 2023 Pavel Pisa <pisa@cmp.fel.cvut.cz>
--
-- based on LX_RoCoN LXPWR and Raspberry Pi BLDC/PMSM motor
-- control design for RPi-MI-1 board desiged by Petr Porazil
-- for PiKRON Ltd <http://www.pikron.com>
--
-- RPi HDL design port has been done by
-- (c) 2015 Martin Prudek <prudemar@fel.cvut.cz>
-- Czech Technical University in Prague
--
-- Project supervision and original project idea
-- idea by Pavel Pisa <pisa@cmp.felk.cvut.cz>
--
-- VHDL design reuses some components and concepts from
-- LXPWR motion power stage board and LX_RoCoN system
-- developed at PiKRON Ltd with base code implemented
-- by Marek Peca <hefaistos@gmail.com>
--
-- license: GNU LGPL and GPLv3+
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity ice_v_ice40_pmsm is
generic(
	pwm_width : natural:=11
	);
port (
	clk_12MHz: in std_logic;

	spi_miso: out std_logic; -- SPI0MISO     (spi_miso)
	spi_mosi: in std_logic; -- SPI0MOSI     (spi_mosi)
	spi_sclk: in std_logic; -- SPI0SCLK     (spi_clk)
	spi_csl: in std_logic; -- SPI0CE1       (spi_ce)

	--
	-- PWM
	-- Each PWM signal has cooresponding shutdown
	pwm: out std_logic_vector (1 to 3);
	shdn: out std_logic_vector (1 to 3);
	-- HAL inputs
	hal_in: in std_logic_vector (1 to 3);
	-- IRC inputs
	irc_a: in std_logic;
	irc_b: in std_logic;
	irc_i: in std_logic;
	-- ADC for current
	adc_miso: in std_logic;
	adc_mosi: out std_logic;
	adc_sclk: out std_logic;
	adc_scs: out std_logic
);
end ice_v_ice40_pmsm;

architecture behavioral of ice_v_ice40_pmsm is
	attribute syn_noprune :boolean;
	attribute syn_preserve :boolean;
	attribute syn_keep :boolean;
	attribute syn_hier :boolean;

	component  ice40_pll
	port (
		clock_in:  in  std_logic;
		clock_out: out std_logic;
		locked:    out std_logic
	);
	end component;

	component qcounter_nbit
	generic (
		bitwidth: integer := 32
	);
	port (
		clock: in std_logic;
		reset: in std_logic;
		a0, b0: in std_logic;
		qcount: out std_logic_vector (31 downto 0);
		a_rise, a_fall, b_rise, b_fall, ab_event: out std_logic;
		ab_error: out std_logic
	);
	end component;

	component mcpwm is
	generic (
		pwm_width: natural
	);
	port (
		clock: in std_logic;
		sync: in std_logic; 				--flag that counter "restarts-overflows"
		data_valid:in std_logic; 			--indicates data is consistent
		failsafe: in std_logic; 			--turn off both transistors
		en_p, en_n: in std_logic; 			--enable positive & enable shutdown
		match: in std_logic_vector (pwm_width-1 downto 0); --posion of counter when we swap output logic
		count: in std_logic_vector (pwm_width-1 downto 0); --we use an external counter
		out_p, out_n: out std_logic 			--pwm outputs: positive & shutdown
		--TODO add the rest of pwm signals, swap match to pwm_word
	);
	end component;

	--frequency division by 12
	component cnt_div is
	generic (
		cnt_width_g : natural := 4
	);
	port
	(
		clk_i     : in std_logic;				--clk to divide
		en_i      : in std_logic;				--enable bit?
		reset_i   : in std_logic;				--asynch. reset
		ratio_i   : in std_logic_vector(cnt_width_g-1 downto 0);--initial value
		q_out_o   : out std_logic				--generates puls when counter underflows
	);
	end component;

	component adc_reader is
	port (
		clk: in std_logic;					--input clk
		divided_clk : in std_logic;				--divided clk - value suitable to sourcing voltage
		adc_reset: in std_logic;
		adc_miso: in std_logic;					--spi master in slave out
		adc_channels: out std_logic_vector (71 downto 0);	--consistent data of 3 channels
		adc_sclk: out std_logic; 				--spi clk
		adc_scs: out std_logic;					--spi slave select
		adc_mosi: out std_logic;				--spi master out slave in
		measur_count: out std_logic_vector(8 downto 0)		--number of accumulated measurments

	);
	end component;

	component dff3 is
	port(
		clk_i   : in std_logic;
		d_i     : in std_logic;
		q_o     : out std_logic
	);
	end component;

	--resetovatelna delicka
	component div128 is
	port (
		clk_in: in std_logic;
		rst: in std_logic;
		fail_safe: out std_logic
	);
	end component;

	component div256 is
	port (
		clk_in: in std_logic;
		div256: out std_logic
	);
	end component;


	signal adc_channels: std_logic_vector(71 downto 0);
	signal adc_m_count: std_logic_vector(8 downto 0);

	--clock signals for logic and master fail monitoring
	signal main_clk: std_logic;
	--signal pll_clkin, pll_clkout, pll_lock: std_logic;
	--signal clkmon_dly1, clkmon_dly2: std_logic;
	--signal clkmon_fail, clkmon_fail_next: std_logic;
	--signal clkmon_wdg: integer range 0 to 6;
	signal reset_sync, reset_async: std_logic;
	signal failsafe, next_failsafe: std_logic;

	--RPi SPI interface signals named aliases
	signal spi_clk, spi_ce : std_logic;
	--signal spi_mosi, spi_miso : std_logic;
	signal spiclk_old: std_logic_vector(1 downto 0); --pro detekci hrany SPI hodin

	--signal pwm_in, pwm_dir_in: std_logic;
	signal dat_reg : STD_LOGIC_VECTOR (127 downto 0); --shift register for spi
	signal position: std_logic_vector(31 downto 0); --pozice z qcounteru
	signal index_position: std_logic_vector(11 downto 0);		--pozice irc_i
	signal ce0_old: std_logic_vector(1 downto 0);

	--pwm signals
	constant pwm_n: natural := 3;					--number of pwm outputs
	--number of ticks per pwm cycle, 2^11=2048
	constant pwm_period : std_logic_vector (pwm_width-1 downto 0) := (others=>'1');
	type pwm_res_type is array(1 to 3) of std_logic_vector (pwm_width-1 downto 0);

	signal pwm_match: pwm_res_type;					--point of reversion of pwm output, 0 to 2047
	signal pwm_count: std_logic_vector (pwm_width-1 downto 0); 	--counter, 0 to 2047
	signal pwm_sync_at_next: std_logic;
	signal pwm_sync: std_logic;
	signal pwm_en_p: std_logic_vector(1 to 3);
	signal pwm_en_n: std_logic_vector(1 to 3);
	signal pwm_sig: std_logic_vector(1 to 3);
	signal shdn_sig: std_logic_vector (1 to 3);

	signal income_data_valid: std_logic;
	signal spi_timout_pulse: std_logic;

	signal clk_4M17: std_logic;

	-- irc signals processing
	signal irc_i_prev: std_logic;

	-- function configuration options
	-- direct 3 phase PWM output
	signal fnccfg_direct_3ph_pwm: std_logic;
	-- PWM1 and PWM2 controlled by PWM input and direction
	signal fnccfg_pwm12_by_pwm_and_dir: std_logic;

	--filetered irc signals
	signal irc_a_dff3: std_logic;
	signal irc_b_dff3: std_logic;

	--hall sensor signals
	signal hal_filt: std_logic_vector(1 to 3);

	--16k3 clk signal
	signal clk_16k3: std_logic;

begin
	-- main_clk <= clk_12MHz;
	reset_async <= '0';

	main_pll: ice40_pll
	port map (
		clock_in => clk_12MHz,
		clock_out => main_clk,
		locked => open
	);

	qcount: qcounter_nbit
	generic map (
		bitwidth => 32
	)
	port map (
		clock => main_clk,
		reset => '0',
		a0 => irc_a_dff3,
		b0 => irc_b_dff3,
		qcount => position,
		a_rise => open,
		a_fall => open,
		b_rise => open,
		b_fall => open,
		ab_event => open,
		ab_error => open
	);

	pwm_block: for i in pwm_n downto 1 generate
		pwm_map: mcpwm
		generic map (
			pwm_width => pwm_width
		)
		port map (
			clock => main_clk, 				--50 Mhz clk from gpclk on raspberry
			sync => pwm_sync,				--counter restarts
			data_valid => pwm_sync_at_next,
			failsafe => failsafe,
			--
			-- pwm config bits & match word
			--
			en_n => pwm_en_n(i),				--enable positive pwm
			en_p => pwm_en_p(i),				--enable "negative" ->activate shutdown
			match => pwm_match(i),
			count => pwm_count,
			-- outputs
			out_p => pwm_sig(i),				--positive signal
			out_n => shdn_sig(i) 				--reverse signal is in shutdown mode
		);
	end generate;

	hal_filt_block: for i in pwm_n downto 1 generate
		dff3_hal: dff3
		port map(
			clk_i => main_clk,
			d_i   => hal_in(i),
			q_o   => hal_filt(i)
		);
	end generate;


	div12_map: cnt_div
	generic map (
		cnt_width_g => 4
	)
	port map(
		clk_i  => main_clk,
		en_i   =>'1',
		reset_i   =>'0',
		ratio_i   =>"1101", --POZN.: counter detekuje cnt<=1
		q_out_o   =>clk_4M17
	);

	clk_16k3_div: cnt_div
	generic map (
		cnt_width_g => 8
	)
	port map(
		clk_i  => main_clk,
		en_i   => clk_4M17,
		reset_i   => '0',
		ratio_i   => "11111111",
		q_out_o   => clk_16k3
	);

	spi_timeout_div : cnt_div
	generic map (
		cnt_width_g => 7
	)
	port map(
		clk_i  => main_clk,
		en_i   => clk_16k3,
		reset_i   => income_data_valid,
		ratio_i   => "1111111",
		q_out_o   => spi_timout_pulse
	);

	-- ADC needs 3.2 MHz clk when powered from +5V Vcc
	--	     2.0 MHz clk when +2.7V Vcc
	-- on the input is 4.17Mhz,but this frequency is divided inside adc_reader by 2 to 2.08 Mhz,
	--        while we use +3.3V Vcc
	adc_reader_map: adc_reader
	port map(
		clk => main_clk,
		divided_clk => clk_4M17,
		adc_reset => income_data_valid, --reset at each SPI cycle,TODO: replace with PLL reset
		adc_miso => adc_miso,
		adc_channels => adc_channels,
		adc_sclk => adc_sclk,
		adc_scs => adc_scs,
		adc_mosi => adc_mosi,
		measur_count => adc_m_count

	);

	dff3_a: dff3
	port map(
		clk_i => main_clk,
		d_i   => irc_a,
		q_o   => irc_a_dff3
	);

	dff3_b: dff3
	port map(
		clk_i => main_clk,
		d_i   => irc_b,
		q_o   => irc_b_dff3
	);

	fnccfg_direct_3ph_pwm <= '0';
	fnccfg_pwm12_by_pwm_and_dir <= '0';

	spi_clk <= spi_sclk;
	spi_ce <= spi_csl;
	-- spi_mosi <= spi_mosi;
	-- spi_miso <= spi_miso;

	pwm <= pwm_sig;
	shdn <= shdn_sig;

	index_process: process
	begin
		wait until (main_clk'event and main_clk='1');
		if irc_i_prev = '0' and irc_i = '1' then
			index_position(11 downto 0)<=position(11 downto 0);
		end if;
		irc_i_prev<=irc_i;
	end process;

	pwm_update_process: process
	begin
		wait until (main_clk'event and main_clk='1');
		if pwm_count = std_logic_vector(unsigned(pwm_period) - 1) then
			--end of period nearly reached
			--fetch new pwm match data
			pwm_sync_at_next <= '1';
		else
			pwm_sync_at_next <= '0';
		end if;

		if pwm_sync_at_next='1' then
			--end of period reached
			pwm_count <= (others=>'0');      --reset counter
			pwm_sync <= '1';       				-- inform PWM logic about new period start
		else						--end of period not reached
			pwm_count <= std_logic_vector(unsigned(pwm_count)+1);		--increment counter
			pwm_sync <= '0';
		end if;
	end process;

	spi_process: process
	begin
		--position is obtained on rising edge -> we should write it on next cycle
		wait until (main_clk'event and main_clk='1');

		--SCLK edge detection
		spiclk_old(0)<=spi_clk;
		spiclk_old(1)<=spiclk_old(0);

		--SS edge detection
		ce0_old(0)<=spi_ce;
		ce0_old(1)<=ce0_old(0);

		if (spiclk_old="01") then --rising edge, faze cteni
			if (spi_ce = '0') then             -- SPI CS must be selected
				-- shift serial data into dat_reg on each rising edge
				-- of SCK, MSB first
				dat_reg(127 downto 0) <= dat_reg(126 downto 0) & spi_mosi;
				end if;
		elsif (spiclk_old="10" ) then --falling edge, faze zapisu
			if (spi_ce = '0') then
				spi_miso <= dat_reg(127); --zapisujeme nejdriv MSB
			end if;
		end if;


		if (ce0_old = "10" ) then
			income_data_valid <= '1';
		else
			income_data_valid <= '0';
		end if;

		--sestupna hrana SS, pripravime data pro prenos
		if (ce0_old = "10" ) then
			dat_reg(127 downto 96) <= position(31 downto 0); --pozice
			dat_reg(95 downto 93) <= hal_filt(1 to 3); --halovy sondy
			dat_reg(92 downto 81) <= index_position(11 downto 0); 	--position of irc_i
			dat_reg(80 downto 72) <=adc_m_count(8 downto 0);   	--count of measurments
			--data order schould be: ch2 downto ch0 downto ch1
			dat_reg(71 downto 0) <= adc_channels(71 downto 0); 	--current mesurments
			spi_miso <= position(31); 		--prepare the first bit on SE activation
		elsif (ce0_old = "01") then --rising edge of SS, we should read the data
			pwm_en_p(1 to 3)<=dat_reg(126 downto 124);
			pwm_en_n(1 to 3)<=dat_reg(123 downto 121);
			--usable for up to 16-bit PWM duty cycle resolution (pwm_width):
			pwm_match(1)(pwm_width-1 downto 0)<=dat_reg(pwm_width+31 downto 32);
			pwm_match(2)(pwm_width-1 downto 0)<=dat_reg(pwm_width+15 downto 16);
			pwm_match(3)(pwm_width-1 downto 0)<=dat_reg(pwm_width-1 downto 0);
		end if;
	end process;

	failsafe_spi_monitor: process (failsafe, spi_timout_pulse, income_data_valid)
	begin
		-- the failasfe signal from communication block if CRC is used
		-- or simple watchdog for SPI communication
		if income_data_valid = '1' then
			next_failsafe <= '0';
		elsif spi_timout_pulse = '1' then
			next_failsafe <= '1';
		else
			next_failsafe <= failsafe;
		end if;
	end process;

	async_rst: process (main_clk, reset_async, reset_sync)
	begin
		if reset_async = '1' then
			failsafe <= '1';
		elsif main_clk'event and main_clk = '1' then
			failsafe <= next_failsafe or reset_sync;
		end if;
	end process;

	sync_rst: process (main_clk, reset_async)
	begin
		if main_clk'event and main_clk = '1' then
			reset_sync <= reset_async;
		end if;
	end process;

end behavioral;
