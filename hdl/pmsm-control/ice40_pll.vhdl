library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ice40_pll is
	port (
		clock_in:  in  std_logic;
		clock_out: out std_logic;
		locked:    out std_logic
	);
end ice40_pll;

architecture behavioral of ice40_pll is
  component SB_PLL40_PAD
  generic (
		--- Feedback
		FEEDBACK_PATH	 		: string := "SIMPLE"; -- String (simple, delay, phase_and_delay, external)
		DELAY_ADJUSTMENT_MODE_FEEDBACK 	: string := "FIXED";
		DELAY_ADJUSTMENT_MODE_RELATIVE 	: string := "FIXED";
		SHIFTREG_DIV_MODE 		: bit_vector(1 downto 0)	:= "00"; 	 --  0-->Divide by 4, 1-->Divide by 7, 3 -->Divide by 5
		FDA_FEEDBACK 			: bit_vector(3 downto 0) 	:= "0000"; 	 --  Integer (0-15).
		FDA_RELATIVE 			: bit_vector(3 downto 0)	:= "0000"; 	 --  Integer (0-15).
		PLLOUT_SELECT			: string := "GENCLK";

		--- Use the spread sheet to populate the values below
		DIVF				: bit_vector(6 downto 0);  -- Determine a good default value
		DIVR				: bit_vector(3 downto 0);  -- Determine a good default value
		DIVQ				: bit_vector(2 downto 0);  -- Determine a good default value
		FILTER_RANGE			: bit_vector(2 downto 0);  -- Determine a good default value

		--- Additional C-Bits
		ENABLE_ICEGATE			: bit := '0';

		--- Test Mode Parameter
		TEST_MODE			: bit := '0';
		EXTERNAL_DIVIDE_FACTOR		: integer := 1 -- Not Used by model, Added for PLL config GUI
	);
  port (
        PACKAGEPIN		: in  std_logic;
        PLLOUTCORE		: out std_logic;		    -- PLL output to core logic
        PLLOUTGLOBAL		: out std_logic;		    -- PLL output to global network
        --EXTFEEDBACK		: in std_logic;			    -- Driven by core logic
        DYNAMICDELAY		: in std_logic_vector (7 downto 0); -- Driven by core logic
        LOCK			: out std_logic;		    -- Output of PLL
        BYPASS			: in std_logic;			    -- Driven by core logic
        RESETB			: in std_logic			    -- Driven by core logic
        --LATCHINPUTVALUE	 in std_logic;			    -- iCEGate Signal
        -- Test Pins
        --SDO			: out std_logic;		    -- Output of PLL
        --SDI			: in std_logic;			    -- Driven by core logic
        --SCLK			: in std_logic			    -- Driven by core logic
       );
  end component;

begin

        pll : SB_PLL40_PAD
        generic map (
                FEEDBACK_PATH => "SIMPLE",
                -- DIVR => "0000",         -- DIVR =  0
                -- DIVF => "0111111",      -- DIVF =  64
                -- DIVQ => "100",          -- DIVQ =  4
                DIVR => "0000",         -- DIVR =  0
                DIVF => "1001111",      -- DIVF =  64
                DIVQ => "101",          -- DIVQ =  4
                FILTER_RANGE => "001"   -- FILTER_RANGE = 1

        )
        port map(
                LOCK => locked,
                RESETB => '1',
                BYPASS => '0',
                PACKAGEPIN => clock_in,
                PLLOUTCORE => clock_out,
                DYNAMICDELAY => "00000000"
        );

end behavioral;
