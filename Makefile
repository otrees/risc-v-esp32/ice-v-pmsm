default: all

.PHONY: default all hdl hdl-to-apps nuttx-build omk-apps sw

.NOTPARALLEL:

ICE_V_PMSM_BISTREAM_IN_APPS = \
	sw/nuttx-omk/ice_v_pmsm/romfs/fpga/ice_v_ice40_pmsm.bin \
	sw/pysim/nuttx_spi_pmsm_cl_pid_q/romfs/fpga/ice_v_ice40_pmsm.bin \
	sw/pysim/nuttx_spi_pmsm_align_check/romfs/fpga/ice_v_ice40_pmsm.bin \
	sw/pysim/nuttx_spi_pmsm_cl_curr_d_pid_q/romfs/fpga/ice_v_ice40_pmsm.bin \

hdl:
	make -C hdl/pmsm-control

hdl-to-apps:
	for i in $(ICE_V_PMSM_BISTREAM_IN_APPS) ; do \
	  cp -v hdl/pmsm-control/ice_v_ice40_pmsm.bin $$i ; \
	done

nuttx-build:
	make -C sw/nuttx-build

omk-apps:
	make -C sw/nuttx-omk

sw: nuttx-build omk-apps

all: hdl hdl-to-apps sw

clean:
	rm -f sw/nuttx-omk/config.omk-default
	make -C hdl/pmsm-control clean
	make -C sw/nuttx-omk clean
	make -C sw/nuttx-build clean
